<!DOCTYPE html>
<html>
<head>
	<title>library</title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
	<style type="text/css">
	header{opacity: 0.7;}
	footer{background-color: #fff;opacity: 0.9;text-align: center;}
	</style>
</head>
<body>
	<header class="jumbotron">
		<div class="container">
			<div class="col-md-10">
			<h1>The Bookstore!</h1>
			<p>Reading A good book is like talking a journey.</p>
			</div>
			<div class="col-md-2">
				Data : {{$date}} <br/> Time : {{$time}}
			</div>
		</div>
	</header>



	@yield('content')


	<footer class="container">
		&copy; All Right Reserved for karim-2017
	</footer>

</body>
</html>