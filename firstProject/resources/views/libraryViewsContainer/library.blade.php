@extends('master')
@section('content')

<div class="container" style="opacity:0.9">
	<div class="row">
		@foreach($section as $k=>$v)
		<div class="col-md-3">
			<div class="thumbnail">
				<img src='{{asset("images/$v") }}'>
				<h1><a class="btn btn-primary">{{$k}}</a></h1>
			</div>
		</div>
		@endforeach
	</div>
</div>

@stop