<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/*
Route::get('/',function(){
	return '<center><a href="kimo">Go to kimo</a><br><h1>welcome karim</h1></center>';
});
route::get('kimo',function(){
	return '<center><a href="/">Go To Home</a><br>my name is karim ali<br><img src="https://media.licdn.com/mpr/mpr/shrinknp_200_200/p/7/005/0af/261/09512c0.jpg" alt="aha"></center>';
});
//route::get('user/{anything}',function($anything){
	//return 'you have entered ot attached to the url pattern argument= '. $anything ;
//});
//route::get('users/{anythings}',function($anythings){
//	return 'you have entered ot attached to the url pattern argument= '. $anythings;
//})->where('anythings','[0-9]+');


route::get('/',function(){
	return Redirect::to('kimo');
});
*/
///////////////////////////////////////////////////////////////////////////////////////

/*
Route::get('library',function(){
	return '<center><h1>the library</h1></center>';
});

Route::get('library/create',function(){
	return '<center><h1>Adding New Section to The library</h1></center>';
});

Route::post('library/create',function(){
	$sectionName=Input::get('sectionName');
	$sectionDetails=Input::get('sectionDetails');
});

route::get('library/{sectionName}',function($sectionName){
	return '<center><h1>This page for ' . $sectionName . 'section</h1></center>';
});

Route::get('library/{sectioName}/edit',function($sectioName){
	return '<center><h1>Returning The Form for editing' . $sectioName . 'section</h1></center>';
});

Route::patch('library/{sectioName}/edit',function($sectioName){
	
});
Route::delete('library/{sectioName}/delete',function($sectioName){
	
});
*/
//////////////////////////////////  BASIC CONTROLLER  /////////////////////////////////////////////////////////
/*
Route::get('library','sectionController@index');
Route::get('library/create','sectionController@createNewSection');
Route::post('library/create','sectionController@saveNewSection');
Route::get('library/{sectionName}','sectionController@showSection');
Route::get('library/{sectionName}/edit','sectionController@editSection');
Route::patch('library/{sectionName}/edit','sectionController@saveEditSection');
Route::delete('library/{sectionName}/delete','sectionController@deleteSection');
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////  RESTFUL CONTROLLER  /////////////////////////////////////////////////////////

// Route::controller('library','sectionController');

//////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////  RESOURCE CONTROLLER  /////////////////////////////////////////////////////////
//Route::resource('library','sectionController2');

//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////   TASK    /////////////////////////////////////////////////////////////////////////

route::get('/','sectionController@section1');
route::get('library','sectionController@section2');
route::get('library/1','sectionController@section3');
///////////////////////////////////////////////////////////////////////////////////////////////////////