<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class sectionController2 extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //return '<center><h1> The library Welcome</h1></center>';
        $date=date('Y-m-d');
        $time=date('H:i:s');
        //$arrayofobject=['data'=>$data,'time'=>$time];
        //return view('libraryViewsContainer.library',$arrayofobject);
        //return view('libraryViewsContainer.library',compact('data','time'));
        //return view('libraryViewsContainer.library')->with('data',$data)->with('time',$time);
        $section=['History'=>'history.jpg','Science'=>'science.jpg','Electrical'=>'electrical.jpg','Electorincs'=>'electorincs.jpg'];
        return view('libraryViewsContainer.library')->withDate($date)->withTime($time)->withSection($section);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return '<center><h1> Creating new section in The library !</h1></center>';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //storing the new created section to the  db
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return '<center><h1>you are inside ' . $id . 'section!</h1></center>';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return '<center><h1>you are tyring to edit = ' . $id . 'section!</h1></center>';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //saving  the edited sectionto the db
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //delete the section with id = $id from db
    }
}
